﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webApiDbFaceDB.Models;
using static System.IO.File;

namespace webApiDbFaceDB.Controllers
{
    /// <summary>
    /// This class is a controller for Photo model;
    /// This controller capable of performing CRUD requests.
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        private DatabaseContext DbContext { get; }

        public PhotoController(DatabaseContext dbContext)
        {
            // If Photo table doesn't have any records, then it'll be filled with new entities

            DbContext = dbContext;
            if (DbContext.Photos.Any()) return;

            DbContext.Photos.Add(new Photo()
            {
                PersonId = 1,
                Image = Convert.ToBase64String(ReadAllBytes(Environment.CurrentDirectory + "\\TestPhotos\\photo1.jpg")),
                ImageDateAdded = DateTime.Now
            });
            DbContext.Photos.Add(new Photo()
            {
                PersonId = 2,
                Image = Convert.ToBase64String(ReadAllBytes(Environment.CurrentDirectory + "\\TestPhotos\\photo2.jpg")),
                ImageDateAdded = DateTime.Now
            });
            DbContext.Photos.Add(new Photo()
            {
                PersonId = 2,
                Image = Convert.ToBase64String(ReadAllBytes(Environment.CurrentDirectory + "\\TestPhotos\\photo2.jpg")),
                ImageDateAdded = DateTime.Now
            });
            DbContext.Photos.Add(new Photo()
            {
                PersonId = 3,
                Image = Convert.ToBase64String(ReadAllBytes(Environment.CurrentDirectory + "\\TestPhotos\\photo3.jpg")),
                ImageDateAdded = DateTime.Now
            });

            DbContext.SaveChanges();
        }

        /// <summary>
        /// This method is responsible for GET / SELECT request;
        /// Gets list of the photo entities from the database.
        /// </summary>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/photo/ -Method GET
        /// </example>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Photo>>> GetPersonList()
        {
            return await DbContext.Photos.ToListAsync();
        }

        /// <summary>
        /// This method is responsible for GET / SELECT request;
        /// Gets the photo entity from the database by Id.
        /// </summary>
        ///
        /// <param name="id">Photo identification number</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/photo/3 -Method GET
        /// </example>
        [HttpGet("{id}")]
        public async Task<ActionResult<Photo>> Get(int id)
        {
            var photo = await DbContext.Photos.FirstOrDefaultAsync(x => x.Id == id);
            if (photo == null)
                return NotFound();
            return new ObjectResult(photo);
        }

        /// <summary>
        /// This method is responsible for POST / INSERT request;
        /// Inserts the photo entity into the database.
        /// </summary>
        ///
        /// <param name="photo">An entity of Photo class</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/photo -Method POST -Body (@{PersonId = 3; Image = "IMAGE_BASE64"; ImageDateAdded = "2019-01-06T17:16:40"} | ConvertTo-Json) -ContentType "application/json; charset=utf-8"
        /// </example>
        [HttpPost]
        public async Task<ActionResult<Photo>> Post(Photo photo)
        {
            if (photo == null) return BadRequest();

            await DbContext.Photos.AddAsync(photo);
            await DbContext.SaveChangesAsync();
            return Ok(photo);
        }

        /// <summary>
        /// This method is responsible for PUT / UPDATE request;
        /// Updates the photo entity into the database by Id.
        /// </summary>
        ///
        /// <param name="photo">An entity of Photo class</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/photo -Method PUT -Body (@{Id = 2002; PersonId = 3; Image = "iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAPwSURBVHhe7dTBCYBAAAPBaP89qw+LuIUZCOlgr23PN4Dj3f8DHE+wgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECIrYXbbMDV2o0mg0AAAAASUVORK5CYII="; ImageDateAdded = "2019-01-06T17:16:40"} | ConvertTo-Json) -ContentType "application/json; charset=utf-8"
        /// </example>
        [HttpPut]
        public async Task<ActionResult<Photo>> Put(Photo photo)
        {
            if (photo == null) return BadRequest();
            if (!DbContext.Photos.Any(x => x.Id == photo.Id)) return NotFound();

            DbContext.Update(photo);
            await DbContext.SaveChangesAsync();
            return Ok(photo);
        }

        /// <summary>
        /// This method is responsible for DELETE request;
        /// Removes the photo entity from the database by Id.
        /// </summary>
        ///
        /// <param name="id">Photo identification number</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/photo/1003 -Method DELETE
        /// </example>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Photo>> Delete(int id)
        {
            var photo = DbContext.Photos.FirstOrDefault(x => x.Id == id);
            if (photo == null) return NotFound();
            DbContext.Photos.Remove(photo);
            await DbContext.SaveChangesAsync();
            return Ok(photo);
        }
    }
}