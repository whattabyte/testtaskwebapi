﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webApiDbFaceDB.Models;

namespace webApiDbFaceDB.Controllers
{
    /// <summary>
    /// This class is a controller for Person model;
    /// This controller capable of performing CRUD requests.
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private DatabaseContext DbContext { get; }

        public PersonController(DatabaseContext dbContext)
        {
            // If Person table doesn't have any records, then it'll be filled with new entities

            DbContext = dbContext;
            if (DbContext.Persons.Any()) return;

            DbContext.Persons.Add(new Person()
            { FirstName = "Valentina", LastName = "Scar-Lo", Patronymic = "Petrova" });
            DbContext.Persons.Add(new Person()
            { FirstName = "Vladimir", LastName = "Golubcow", Patronymic = "Alekseevich" });
            DbContext.Persons.Add(new Person()
            { FirstName = "Fernando", LastName = "De-La", Patronymic = "Cruz" });

            DbContext.SaveChanges();
        }

        /// <summary>
        /// This method is responsible for GET / SELECT request;
        /// Gets list of the person entities from the database.
        /// </summary>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/person/ -Method GET
        /// </example>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Person>>> GetPhotoList()
        {
            return await DbContext.Persons.ToListAsync();
        }

        /// <summary>
        /// This method is responsible for GET / SELECT request;
        /// Gets the person entity from the database by Id.
        /// </summary>
        ///
        /// <param name="id">Person identification number</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/person/3 -Method GET
        /// </example>
        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPersonById(int id)
        {
            var person = await DbContext.Persons.FirstOrDefaultAsync(x => x.Id == id);
            if (person == null)
                return NotFound();
            return new ObjectResult(person);
        }

        /// <summary>
        /// This method is responsible for GET / SELECT request;
        /// Selects all the photos of the Person entity from the database.
        /// </summary>
        ///
        /// <param name="id">Person identification number</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/person/2/photo/ -Method GET
        /// </example>
        [HttpGet("{id}/photo/")]
        public async Task<ActionResult<Person>> GetPhotosByPersonId(int id)
        {
            var person = await DbContext.Persons.FirstOrDefaultAsync(x => x.Id == id);
            if (person == null)
                return NotFound();

            var photos = await DbContext.Photos.Where(x => x.PersonId == id).ToListAsync();

            return new ObjectResult(photos);
        }

        /// <summary>
        /// This method is responsible for GET / SELECT request;
        /// Selects certain photo of the Person entity from the database.
        /// </summary>
        ///
        /// <param name="personId">Person identification number</param>
        /// <param name="photoId">Photo identification number</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/person/2/photo/3 -Method GET
        /// </example>
        [HttpGet("{personId}/photo/{photoId}")]
        public async Task<ActionResult<Person>> GetPhotoByPersonId(int personId, int photoId)
        {
            var person = await DbContext.Persons.FirstOrDefaultAsync(x => x.Id == personId);
            if (person == null)
                return NotFound();

            var photos = await DbContext.Photos.FirstOrDefaultAsync(x => x.PersonId == personId && x.Id == photoId);

            if (photos == null)
                return NotFound();

            return new ObjectResult(photos);
        }

        /// <summary>
        /// This method is responsible for POST / INSERT request;
        /// Inserts the person entity into the database.
        /// </summary>
        ///
        /// <param name="person">An entity of Person class</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/person -Method POST -Body (@{FirstName = "Bobie"; LastName = "Green"; Patronymic = "Moore"} | ConvertTo-Json) -ContentType "application/json; charset=utf-8"
        /// </example>
        [HttpPost]
        public async Task<ActionResult<Person>> Post(Person person)
        {
            if (person == null) return BadRequest();

            await DbContext.Persons.AddAsync(person);
            await DbContext.SaveChangesAsync();
            return Ok(person);
        }

        /// <summary>
        /// This method is responsible for PUT / UPDATE request;
        /// Updates the person entity into the database by Id.
        /// </summary>
        ///
        /// <param name="person">An entity of Person class</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/person/ -Method PUT -Body (@{Id = 3; FirstName = "Robert"; LastName = "Green"; Patronymic = "Moore"} | ConvertTo-Json) -ContentType "application/json"
        /// </example>
        [HttpPut]
        public async Task<ActionResult<Person>> Put(Person person)
        {
            if (person == null) return BadRequest();
            if (!DbContext.Persons.Any(x => x.Id == person.Id)) return NotFound();

            DbContext.Update(person);
            await DbContext.SaveChangesAsync();
            return Ok(person);
        }

        /// <summary>
        /// This method is responsible for DELETE request;
        /// Removes the person entity from the database by Id.
        /// </summary>
        ///
        /// <param name="id">Person identification number</param>
        ///
        /// <example>
        /// Invoke-RestMethod https://localhost:44347/api/person/4 -Method DELETE
        /// </example>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Person>> Delete(int id)
        {
            var person = DbContext.Persons.FirstOrDefault(x => x.Id == id);
            if (person == null) return NotFound();

            DbContext.Persons.Remove(person);
            await DbContext.SaveChangesAsync();
            return Ok(person);
        }
    }
}