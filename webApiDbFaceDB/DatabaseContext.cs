﻿using Microsoft.EntityFrameworkCore;
using webApiDbFaceDB.Models;

namespace webApiDbFaceDB
{
    /// <summary>
    /// This class means the context of the database
    /// and required for EF Core, to interact with the database.
    /// </summary>
    public sealed class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            // Checks if the database if already created
            // and if not - then it's gonna create the database
            Database.EnsureCreated();
        }

        // This is a objects collections that maps to a specific table in the database
        public DbSet<Person> Persons { get; set; }

        public DbSet<Photo> Photos { get; set; }
    }
}