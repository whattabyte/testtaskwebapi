﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webApiDbFaceDB.Models
{
    /// <summary>
    /// This class has an abstract image model / entity
    /// </summary>
    public class Photo
    {
        public int Id { get; set; } // Photo identification number

        [Required]
        public int PersonId { get; set; } // Person's identification number on the photo

        public Person Person { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Image { get; set; } // Image string (base64)

        [Required]
        public DateTime ImageDateAdded { get; set; } // Date the image was added
    }
}