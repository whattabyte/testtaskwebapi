using System.ComponentModel.DataAnnotations;

namespace webApiDbFaceDB.Models
{
    /// <summary>
    /// This class has an abstract person model / entity
    /// </summary>
    public class Person
    {
        public int Id { get; set; } // Identification number

        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; } // Person's first name

        [Required]
        [MaxLength(100)]
        public string LastName { get; set; } // Person's last name

        [MaxLength(100)]
        public string Patronymic { get; set; } // Person's patronymic (father's given name)
    }
}